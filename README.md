# biletado

Backend Implementation of reservations for biletado

# What has been implemented (completed or at least partially completed)
Containers: A simple php cli docker container is used for the backend

Framework: The framework chosen for the backend is Laravel (https://laravel.com/)

Queries: Secured through ORM

JWT: Efforts have been made :S

MIT License included

Gitlab repository linked (https://gitlab.com/NicoSchrodt/biletado; https://gitlab.com/NicoSchrodt/compose)

Logging: Simple connection-related issues are written to stdout and stderr

API: Get and Post, as well as Get, Post and Delete with ID have been implemented. Working with different levels of success. Some input validation (f.e. for get without id)