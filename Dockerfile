FROM php:8.1.6-cli
RUN apt-get update -y && apt-get install -y libmcrypt-dev zlib1g-dev libpng-dev libpq-dev
RUN docker-php-ext-configure pgsql -with-pgsql=/usr/local/pgsql
RUN docker-php-ext-install pdo pdo_pgsql pgsql
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

WORKDIR /app
COPY ./source /app

RUN composer install
EXPOSE 9000
CMD php -d variables_order=EGPCS -S 0.0.0.0:9000 public/index.php