<?php

use App\Http\Controllers\ReservationController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group(['prefix' => 'reservations'], function() {
    Route::get(uri: '/', action: [ReservationController::class, 'search']);
    Route::post(uri: '/', action: [ReservationController::class, 'store']);
    Route::group(['middleware' => 'jwt'], function() {
        Route::get(uri: '/{id}', action: [ReservationController::class, 'searchByID']);
        Route::put(uri: '/{id}', action: [ReservationController::class, 'storeWithID']);
        Route::delete(uri: '/{id}', action: [ReservationController::class, 'removeByID']);
    });
});
