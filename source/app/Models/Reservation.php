<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Reservation extends Model
{
    protected $table = 'reservations';
    public $timestamps = false;
    protected $fillable = ['from', 'to', 'room_id'];
    protected $primaryKey = 'id';
    protected $keyType = 'string';
}
