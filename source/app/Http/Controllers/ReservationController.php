<?php

namespace App\Http\Controllers;

use App\Http\Resources\ReservationResource;
use App\Models\Reservation;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Js;

class ReservationController extends Controller
{
    public function search(Request $request): JsonResponse
    {
        if (isset($request['room_id'])) {
            $item = Reservation::where('room_id', $request['room_id']);
        }
        if (isset($request['before'])) {
            if (isset($item)){
                $item = $item->where('before', '>=', $request['from']);
            } else {
                $item = Reservation::where('before', '>=', $request['from']);
            }
        }
        if (isset($request['after'])) {
            if (isset($item)){
                $item = $item->where('before', '<=', $request['to']);
            } else {
                $item = Reservation::where('before', '<=', $request['to']);
            }
        }
        if (isset($item)) {
            return $this->returnReservations($item->get());
        }

        return $this->returnReservations(Reservation::all());
    }

    //TODO: check for room_id, from and to (NOT OPTIONAL)
    public function store(Request $request): JsonResponse
    {
        $validated = $request->validate([
            'id' => 'uuid|nullable',
            'from' => 'required',
            'to' => 'required',
            'room_id' => 'uuid|required'
        ]);
        return $this->returnReservation(reservation: Reservation::create($validated), statusCode: 201);
    }

    public function searchByID(string $id): JsonResponse
    {
        return $this->returnReservation(Reservation::findOrFail($id));
    }

    public function storeWithID(Request $request, string $id): JsonResponse
    {
        $validated = $request->validate([
            'id' => 'uuid|required',
            'from' => 'required',
            'to' => 'required',
            'room_id' => 'uuid|required'
        ]);
        return $this->returnReservation(reservation: Reservation::create($validated), statusCode: 201);
    }

    public function removeByID(string $id): void
    {
        $reservation = Reservation::findOrFail($id);
        $reservation->delete();
        abort(204);
    }

    private function returnReservation(Reservation $reservation, int $statusCode=200): JsonResponse
    {
        return (new ReservationResource($reservation))->response()->setStatusCode(200);
    }

    private function returnReservations(mixed $reservations): JsonResponse
    {
        return (ReservationResource::collection(resource: $reservations))->response()->setStatusCode(200);
    }
}
